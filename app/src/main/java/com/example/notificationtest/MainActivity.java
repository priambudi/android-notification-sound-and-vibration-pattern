package com.example.notificationtest;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.media.AudioAttributes;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.util.concurrent.atomic.AtomicInteger;
import static com.example.notificationtest.MyBroadcastReceiver.ACTION_SNOOZE;
import static com.example.notificationtest.MyBroadcastReceiver.EXTRA_NOTIFICATION_ID;

public class MainActivity extends AppCompatActivity {

    private static final String CHANNEL_ID_WORLD = "CHANNEL_ID_1";
    private static final String CHANNEL_ID_TOKO = "CHANNEL_ID_2";
    private static final String CHANNEL_ID_SHOP = "CHANNEL_ID_3";
    private AtomicInteger worldId = new AtomicInteger(0);
    private AtomicInteger tokoId = new AtomicInteger(0);
    private AtomicInteger shopId = new AtomicInteger(0);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button hello = findViewById(R.id.hello);
        Button toko = findViewById(R.id.toko);
        Button shop = findViewById(R.id.shop);

        createNotificationChannel(
                CHANNEL_ID_WORLD,
                getString(R.string.channel_name_1),
                NotificationManager.IMPORTANCE_HIGH,
                Uri.parse("android.resource://"+getApplicationContext().getPackageName()+"/"+R.raw.toko),
                new long[] { 1000, 1000, 1000, 1000, 1000 }
        );

        createNotificationChannel(
                CHANNEL_ID_TOKO,
                getString(R.string.channel_name_2),
                NotificationManager.IMPORTANCE_NONE,
                Uri.parse("android.resource://"+getApplicationContext().getPackageName()+"/"+R.raw.toko),
                new long[] { 500, 500, 500, 500, 500 }
        );

        createNotificationChannel(
                CHANNEL_ID_SHOP,
                getString(R.string.channel_name_3),
                NotificationManager.IMPORTANCE_MAX,
                Uri.parse("android.resource://"+getApplicationContext().getPackageName()+"/"+R.raw.shop),
                new long[] { 1000, 500, 500, 500, 1000 }
        );

        hello.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        sendNotification("Dio!", worldId.incrementAndGet());
                    }
                }, 500);
            }
        });

        toko.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        sendNotification("Kuliah Cermat, Bayarnya Hemat!", tokoId.incrementAndGet());
                    }
                }, 500);
            }
        });

        shop.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        sendNotification("Gratis Ongkir Xtra s/d 0RB!", shopId.incrementAndGet());
                    }
                }, 500);
            }
        });
    }

    private void createNotificationChannel(String id, String channelName, int notificationImportance, Uri sound, long[] vibratePattern) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = channelName;
            String description = Integer.toString(Build.VERSION.SDK_INT);

            int importance = notificationImportance;
            AudioAttributes attributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .build();

            NotificationChannel channel = new NotificationChannel(id, name, importance);
            channel.setDescription(description);
            channel.enableLights(true);
            channel.enableVibration(true);
            channel.setVibrationPattern(vibratePattern);
            channel.setSound(sound, attributes);

            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    void sendNotification(String title, int notificationId) {

        Intent snoozeIntent = new Intent(getApplicationContext(), MyBroadcastReceiver.class);
        snoozeIntent.setAction(ACTION_SNOOZE);
        snoozeIntent.putExtra(EXTRA_NOTIFICATION_ID, notificationId);

        PendingIntent snoozePendingIntent =
                PendingIntent.getBroadcast(getApplicationContext(), notificationId, snoozeIntent, 0);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(), CHANNEL_ID_WORLD)
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setContentTitle(title)
                .setContentText("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean tristique turpis ac justo feugiat, in aliquet turpis suscipit. Aenean gravida hendrerit magna, sit amet vehicula lacus accumsan id. Suspendisse ac suscipit dui. Aenean nec feugiat elit. Curabitur cursus placerat nibh, ac commodo ex rutrum eu. Nullam eu venenatis eros, id scelerisque dolor. Fusce id ex a nibh rhoncus facilisis id sodales arcu. Aenean et nunc vulputate, luctus velit sed, euismod ex. Nam hendrerit metus ipsum, eu ornare metus molestie ut.")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setAutoCancel(false)
                .addAction(R.drawable.ic_launcher_foreground, getApplicationContext().getString(R.string.snooze),
                        snoozePendingIntent);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(getApplicationContext());

        notificationManager.notify(notificationId, builder.build());
    }
}
